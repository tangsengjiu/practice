<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/3/11
 * Time: 16:33
 */
Yii::app()->clientScript->registerCssFile("{$this->getAssetsDir()}/css/bootstrap.css?v=" . STATIC_VER);
Yii::app()->clientScript->registerCssFile("{$this->getAssetsDir()}/css/common.css?v=" . STATIC_VER);
if ($this->jsMain)
{
	Yii::app()->clientScript->registerCoreScript('requirejs');
	Yii::app()->clientScript->registerScript('require#main', "require.config({baseUrl: '{$this->getJsDir()}/'});require(['{$this->jsMain}']);", CClientScript::POS_END);
//	Yii::app()->clientScript->registerScriptFile("{$this->getJsDir()}/{$this->getJsMain()}.js", CClientScript::POS_HEAD);
}
Yii::app()->clientScript->registerScriptFile("{$this->getJsDir()}/error.js", CClientScript::POS_HEAD);
?>
<?=$content?>
