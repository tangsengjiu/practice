<?php
/**
 * Created by PhpStorm.
 * User: liujing
 * Date: 2016/5/4
 * Time: 23:25
 */

class ServerController  extends CController
{
    public function getServerInfo(){
        switch (PHP_OS) {
            case "Linux":
                $sysReShow = (false !== ($sysInfo = ServerController::sys_linux())) ? "show" : "none";
                break;
            case "FreeBSD":
                $sysReShow = (false !== ($sysInfo = ServerController::sys_freebsd())) ? "show" : "none";
                break;
            default:
                break;
        }
        $uptime = $sysInfo['uptime']; //在线时间
        $stime  = date('Y-m-d H:i:s'); //系统当前时间
        //硬盘
        $dt        = round(@disk_total_space(".") / (1024 * 1024 * 1024), 3); //总
        $df        = round(@disk_free_space(".") / (1024 * 1024 * 1024), 3); //可用
        $du        = $dt - $df; //已用
        $hdPercent = (floatval($dt) != 0) ? round($du / $dt * 100, 2) : 0;
        $load      = $sysInfo['loadAvg']; //系统负载
        //判断内存如果小于1G，就显示M，否则显示G单位
        if ($sysInfo['memTotal'] < 1024) {
            $memTotal         = $sysInfo['memTotal'] . " M";
            $mt               = $sysInfo['memTotal'] . " M";
            $mu               = $sysInfo['memUsed'] . " M";
            $mf               = $sysInfo['memFree'] . " M";
            $mc               = $sysInfo['memCached'] . " M"; //cache化内存
            $mb               = $sysInfo['memBuffers'] . " M"; //缓冲
            $st               = $sysInfo['swapTotal'] . " M";
            $su               = $sysInfo['swapUsed'] . " M";
            $sf               = $sysInfo['swapFree'] . " M";
            $swapPercent      = $sysInfo['swapPercent'];
            $memRealUsed      = $sysInfo['memRealUsed'] . " M"; //真实内存使用
            $memRealFree      = $sysInfo['memRealFree'] . " M"; //真实内存空闲
            $memRealPercent   = $sysInfo['memRealPercent']; //真实内存使用比率
            $memPercent       = $sysInfo['memPercent']; //内存总使用率
            $memCachedPercent = $sysInfo['memCachedPercent']; //cache内存使用率
        } else {
            $memTotal         = round($sysInfo['memTotal'] / 1024, 3) . " G";
            $mt               = round($sysInfo['memTotal'] / 1024, 3) . " G";
            $mu               = round($sysInfo['memUsed'] / 1024, 3) . " G";
            $mf               = round($sysInfo['memFree'] / 1024, 3) . " G";
            $mc               = round($sysInfo['memCached'] / 1024, 3) . " G";
            $mb               = round($sysInfo['memBuffers'] / 1024, 3) . " G";
            $st               = round($sysInfo['swapTotal'] / 1024, 3) . " G";
            $su               = round($sysInfo['swapUsed'] / 1024, 3) . " G";
            $sf               = round($sysInfo['swapFree'] / 1024, 3) . " G";
            $swapPercent      = $sysInfo['swapPercent'];
            $memRealUsed      = round($sysInfo['memRealUsed'] / 1024, 3) . " G"; //真实内存使用
            $memRealFree      = round($sysInfo['memRealFree'] / 1024, 3) . " G"; //真实内存空闲
            $memRealPercent   = $sysInfo['memRealPercent']; //真实内存使用比率
            $memPercent       = $sysInfo['memPercent']; //内存总使用率
            $memCachedPercent = $sysInfo['memCachedPercent']; //cache内存使用率
        }
        //网卡流量
        $strs = @file("/proc/net/dev");
        for ($i = 2; $i < count($strs); $i++) {
            preg_match_all("/([^\s]+):[\s]{0,}(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)/", $strs[$i], $info);
            $NetOutSpeed[$i]   = $info[10][0];
            $NetInputSpeed[$i] = $info[2][0];
            $NetInput[$i]      = ServerController::formatsize($info[2][0]);
            $NetOut[$i]        = ServerController::formatsize($info[10][0]);
        }
        //ajax调用实时刷新
        $arr = array('useSpace'            => $du,
            'freeSpace'           => $df,
            'hdPercent'           => $hdPercent,
            'barhdPercent'        => $hdPercent . '%',
            'TotalMemory'         => $mt,
            'UsedMemory'          => $mu,
            'FreeMemory'          => $mf,
            'CachedMemory'        => $mc,
            'Buffers'             => $mb,
            'TotalSwap'           => $st,
            'swapUsed'            => $su,
            'swapFree'            => $sf,
            'loadAvg'             => $load,
            'uptime'              => $uptime,
            'freetime'            => $freetime,
            'bjtime'              => $bjtime,
            'stime'               => $stime,
            'memRealPercent'      => $memRealPercent,
            'memRealUsed'         => $memRealUsed,
            'memRealFree'         => $memRealFree,
            'memPercent'          => $memPercent . '%',
            'memCachedPercent'    => $memCachedPercent,
            'barmemCachedPercent' => $memCachedPercent . '%',
            'swapPercent'         => $swapPercent,
            'barmemRealPercent'   => $memRealPercent . '%',
            'barswapPercent'      => $swapPercent . '%',
            'NetOut2'             => $NetOut[2],
            'NetOut3'             => $NetOut[3],
            'NetOut4'             => $NetOut[4],
            'NetOut5'             => $NetOut[5],
            'NetOut6'             => $NetOut[6],
            'NetOut7'             => $NetOut[7],
            'NetOut8'             => $NetOut[8],
            'NetOut9'             => $NetOut[9],
            'NetOut10'            => $NetOut[10],
            'NetInput2'           => $NetInput[2],
            'NetInput3'           => $NetInput[3],
            'NetInput4'           => $NetInput[4],
            'NetInput5'           => $NetInput[5],
            'NetInput6'           => $NetInput[6],
            'NetInput7'           => $NetInput[7],
            'NetInput8'           => $NetInput[8],
            'NetInput9'           => $NetInput[9],
            'NetInput10'          => $NetInput[10],
            'NetOutSpeed2'        => $NetOutSpeed[2],
            'NetOutSpeed3'        => $NetOutSpeed[3],
            'NetOutSpeed4'        => $NetOutSpeed[4],
            'NetOutSpeed5'        => $NetOutSpeed[5],
            'NetInputSpeed2'      => $NetInputSpeed[2],
            'NetInputSpeed3'      => $NetInputSpeed[3],
            'NetInputSpeed4'      => $NetInputSpeed[4],
            'NetInputSpeed5'      => $NetInputSpeed[5]);

        $retMsg->retcode = 0;
        $retMsg->message = $arr;
        Common::dieWithJSONP($retMsg);
    }
    //linux系统探测
    function sys_linux() {
        // CPU
        if (false === ($str = @file("/proc/cpuinfo")))
            return false;
        $str = implode("", $str);
        @preg_match_all("/model\s+name\s{0,}\:+\s{0,}([\w\s\)\(\@.-]+)([\r\n]+)/s", $str, $model);
        @preg_match_all("/cpu\s+MHz\s{0,}\:+\s{0,}([\d\.]+)[\r\n]+/", $str, $mhz);
        @preg_match_all("/cache\s+size\s{0,}\:+\s{0,}([\d\.]+\s{0,}[A-Z]+[\r\n]+)/", $str, $cache);
        @preg_match_all("/bogomips\s{0,}\:+\s{0,}([\d\.]+)[\r\n]+/", $str, $bogomips);
        if (false !== is_array($model[1])) {
            $res['cpu']['num'] = sizeof($model[1]);
            /*
             for($i = 0; $i < $res['cpu']['num']; $i++)
             {
             $res['cpu']['model'][] = $model[1][$i].'&nbsp;('.$mhz[1][$i].')';
             $res['cpu']['mhz'][] = $mhz[1][$i];
             $res['cpu']['cache'][] = $cache[1][$i];
             $res['cpu']['bogomips'][] = $bogomips[1][$i];
             }*/
            if ($res['cpu']['num'] == 1)
                $x1 = ''; else
                $x1 = ' ×' . $res['cpu']['num'];
            $mhz[1][0]             = ' | 频率:' . $mhz[1][0];
            $cache[1][0]           = ' | 二级缓存:' . $cache[1][0];
            $bogomips[1][0]        = ' | Bogomips:' . $bogomips[1][0];
            $res['cpu']['model'][] = $model[1][0] . $mhz[1][0] . $cache[1][0] . $bogomips[1][0] . $x1;
            if (false !== is_array($res['cpu']['model']))
                $res['cpu']['model'] = implode("<br />", $res['cpu']['model']);
            if (false !== is_array($res['cpu']['mhz']))
                $res['cpu']['mhz'] = implode("<br />", $res['cpu']['mhz']);
            if (false !== is_array($res['cpu']['cache']))
                $res['cpu']['cache'] = implode("<br />", $res['cpu']['cache']);
            if (false !== is_array($res['cpu']['bogomips']))
                $res['cpu']['bogomips'] = implode("<br />", $res['cpu']['bogomips']);
        }
        // NETWORK
        // UPTIME
        if (false === ($str = @file("/proc/uptime")))
            return false;
        $str   = explode(" ", implode("", $str));
        $str   = trim($str[0]);
        $min   = $str / 60;
        $hours = $min / 60;
        $days  = floor($hours / 24);
        $hours = floor($hours - ($days * 24));
        $min   = floor($min - ($days * 60 * 24) - ($hours * 60));
        if ($days !== 0)
            $res['uptime'] = $days . "天";
        if ($hours !== 0)
            $res['uptime'] .= $hours . "小时";
        $res['uptime'] .= $min . "分钟";
        // MEMORY
        if (false === ($str = @file("/proc/meminfo")))
            return false;
        $str = implode("", $str);
        preg_match_all("/MemTotal\s{0,}\:+\s{0,}([\d\.]+).+?MemFree\s{0,}\:+\s{0,}([\d\.]+).+?Cached\s{0,}\:+\s{0,}([\d\.]+).+?SwapTotal\s{0,}\:+\s{0,}([\d\.]+).+?SwapFree\s{0,}\:+\s{0,}([\d\.]+)/s", $str, $buf);
        preg_match_all("/Buffers\s{0,}\:+\s{0,}([\d\.]+)/s", $str, $buffers);
        $res['memTotal']         = round($buf[1][0] / 1024, 2);
        $res['memFree']          = round($buf[2][0] / 1024, 2);
        $res['memBuffers']       = round($buffers[1][0] / 1024, 2);
        $res['memCached']        = round($buf[3][0] / 1024, 2);
        $res['memUsed']          = $res['memTotal'] - $res['memFree'];
        $res['memPercent']       = (floatval($res['memTotal']) != 0) ? round($res['memUsed'] / $res['memTotal'] * 100, 2) : 0;
        $res['memRealUsed']      = $res['memTotal'] - $res['memFree'] - $res['memCached'] - $res['memBuffers']; //真实内存使用
        $res['memRealFree']      = $res['memTotal'] - $res['memRealUsed']; //真实空闲
        $res['memRealPercent']   = (floatval($res['memTotal']) != 0) ? round($res['memRealUsed'] / $res['memTotal'] * 100, 2) : 0; //真实内存使用率
        $res['memCachedPercent'] = (floatval($res['memCached']) != 0) ? round($res['memCached'] / $res['memTotal'] * 100, 2) : 0; //Cached内存使用率
        $res['swapTotal']        = round($buf[4][0] / 1024, 2);
        $res['swapFree']         = round($buf[5][0] / 1024, 2);
        $res['swapUsed']         = round($res['swapTotal'] - $res['swapFree'], 2);
        $res['swapPercent']      = (floatval($res['swapTotal']) != 0) ? round($res['swapUsed'] / $res['swapTotal'] * 100, 2) : 0;
        // LOAD AVG
        if (false === ($str = @file("/proc/loadavg")))
            return false;
        $str            = explode(" ", implode("", $str));
        $str            = array_chunk($str, 4);
        $res['loadAvg'] = implode(" ", $str[0]);
        return $res;
    }

    //单位转换
    function formatsize($size) {
        $danwei  = array(' B ',
            ' K ',
            ' M ',
            ' G ',
            ' T ');
        $allsize = array();
        $i       = 0;
        for ($i = 0; $i < 5; $i++) {
            if (floor($size / pow(1024, $i)) == 0) {
                break;
            }
        }
        for ($l = $i - 1; $l >= 0; $l--) {
            $allsize1[$l] = floor($size / pow(1024, $l));
            $allsize[$l]  = $allsize1[$l] - $allsize1[$l + 1] * 1024;
        }
        $len = count($allsize);
        for ($j = $len - 1; $j >= 0; $j--) {
            $fsize = $fsize . $allsize[$j] . $danwei[$j];
        }
        return $fsize;
    }
    //FreeBSD系统探测
    function sys_freebsd() {
        //CPU
        if (false === ($res['cpu']['num'] = get_key("hw.ncpu")))
            return false;
        $res['cpu']['model'] = get_key("hw.model");
        //LOAD AVG
        if (false === ($res['loadAvg'] = get_key("vm.loadavg")))
            return false;
        //UPTIME
        if (false === ($buf = get_key("kern.boottime")))
            return false;
        $buf       = explode(' ', $buf);
        $sys_ticks = time() - intval($buf[3]);
        $min       = $sys_ticks / 60;
        $hours     = $min / 60;
        $days      = floor($hours / 24);
        $hours     = floor($hours - ($days * 24));
        $min       = floor($min - ($days * 60 * 24) - ($hours * 60));
        if ($days !== 0)
            $res['uptime'] = $days . "天";
        if ($hours !== 0)
            $res['uptime'] .= $hours . "小时";
        $res['uptime'] .= $min . "分钟";
        //MEMORY
        if (false === ($buf = get_key("hw.physmem")))
            return false;
        $res['memTotal'] = round($buf / 1024 / 1024, 2);
        $str = get_key("vm.vmtotal");
        preg_match_all("/\nVirtual Memory[\:\s]*\(Total[\:\s]*([\d]+)K[\,\s]*Active[\:\s]*([\d]+)K\)\n/i",
            $str,
            $buff,
            PREG_SET_ORDER);
        preg_match_all("/\nReal Memory[\:\s]*\(Total[\:\s]*([\d]+)K[\,\s]*Active[\:\s]*([\d]+)K\)\n/i",
            $str,
            $buf,
            PREG_SET_ORDER);
        $res['memRealUsed'] = round($buf[0][2] / 1024, 2);
        $res['memCached']   = round($buff[0][2] / 1024, 2);
        $res['memUsed']     = round($buf[0][1] / 1024, 2) + $res['memCached'];
        $res['memFree']     = $res['memTotal'] - $res['memUsed'];
        $res['memPercent']  = (floatval($res['memTotal']) != 0) ? round($res['memUsed'] / $res['memTotal'] * 100, 2) : 0;
        $res['memRealPercent'] = (floatval($res['memTotal']) != 0) ? round($res['memRealUsed'] / $res['memTotal'] * 100,
            2) : 0;
        return $res;
    }

    /**
     * 获取缓存信息
     */
    public  function  get_memcache_stat() {
        $m = new \Memcache();
        try {
            if (is_callable(array($m,
                'connect'))
            )
                $m->connect('127.0.0.1', 11211);
            $retMsg->retcode = 0;
            $stats           = $m->getstats();
            $str             = $stats['uptime'];
            $min             = $str / 60;
            $hours           = $min / 60;
            $days            = floor($hours / 24);
            $hours           = floor($hours - ($days * 24));
            $min             = floor($min - ($days * 60 * 24) - ($hours * 60));
            if ($days !== 0)
                $stats['uptime'] = $days . "天";
            if ($hours !== 0)
                $stats['uptime'] .= $hours . "小时";
            $stats['uptime'] .= $min . "分钟";
            $retMsg->message = $stats;
        } catch (\Exception $e) {
            $retMsg->retcode = 1;
            $retMsg->message = 'can not connect to memcache';
        }
        \Common::dieWithJSONP($retMsg);
    }
    public  function  getCronList(){
        extract($_REQUEST);
        $this->load->database();
        $sTable   = 'cron';
        $aColumns = array(
            'id',
            'id',
            'name',
            'cycle',
            'callbackurl',
            'last_fetchtime',
            'fetch_count',
            'status',
            'createtime',
            'last_response'
        );
        $sLimit = "";
        $sLimit =  Common::getListSqlLimit($sLimit);
        $sOrder = "";
        $sOrder =   Common::getListSqlOrder($sOrder,$aColumns);
        $sWhere = "";
        $sWhere =  Common::getListSqlWhere($sWhere,$aColumns);

        $sQuery = "
    SELECT SQL_CALC_FOUND_ROWS `" . str_replace(" , ", " ", implode("`, `", $aColumns)) . "`
    FROM   $sTable
    $sWhere
    $sOrder
    $sLimit
";
        $res       = Common::transresult_new($this, $sQuery, $error);
        $sql_count = Common::prepareSql('SELECT COUNT(*) FROM %s %s', $sTable, $sWhere);
        $sql_count = str_replace("\\", "", $sql_count);
        $res_count = Common::transresult_new($this, $sql_count, $error);
        $retCode   = 0;
        $retMsg    = array('sEcho'                => $sEcho,
            'iTotalRecords'        => $res_count[0]['COUNT(*)'],
            "iTotalDisplayRecords" => $res_count[0]['COUNT(*)'],
            'aaData'               => $res);
        Common::dieWithJSONP($retMsg);
    }


    /**
     * 新增cron
     */
    public   function add_cron() {
        $error = null;
        extract($_REQUEST);
        $this->load->database();
        $sql = Common::prepareSql('INSERT INTO `cron` (`name`, `cycle`, `callbackurl`) VALUES ("%s", "%s", "%s")', $name, $cycle, $callbackurl);
        $ret =  $this->db->query($sql, $error);
        if (empty($error)) {
            $retMsg->retcode = 0;
            $retMsg->message = 'success';
        } else {
            $retMsg->retcode = 0;
            $retMsg->message = 'fail';
        }
        Common::dieWithJSONP($retMsg);
    }

    /**
     * 删除 cron
     */
    public   function delete_cron() {
        $error = null;
        extract($_REQUEST);
        $this->load->database();
        $sql = Common::prepareSql('DELETE FROM `cron` WHERE id=%d', $id);
        $ret =$this->db->query($sql, $error);
        if (empty($error)) {
            $retMsg->retcode = 0;
            $retMsg->message = 'success';
        } else {
            $retMsg->retcode = 0;
            $retMsg->message = 'fail';
        }
        Common::dieWithJSONP($retMsg);
    }

    /**
     * 更新 cron
     */
    public   function update_cron() {
        $error = null;
        extract($_REQUEST);
        $this->load->database();
        $sql = Common::prepareSql("update cron set name='%s', cycle='%s',callbackurl='%s'where id=%d", $name, $cycle, $callbackurl, $id);
        $ret = $this->db->query($sql,$error);
        if (empty($error)) {
            $retMsg->retcode = 0;
            $retMsg->message = 'success';
        } else {
            $retMsg->retcode = 0;
            $retMsg->message = 'fail';
        }

        Common::dieWithJSONP($retMsg);
    }

    /**
     * 刷新 cron
     */
    public   function  refresh_cron() {
        $ret  = file_get_contents('http://127.0.0.1:3000/refresh?' . http_build_query(array('auth_key' => AUTH_KEY_CRON)));
        $data = (array)json_decode($ret);
        if (isset($data['retcode'])) {
            $retMsg->retcode = $data['retcode'];
            $retMsg->message = $data['message'];
        } else {
            $retMsg->retcode = 1;
            $retMsg->message = 'cron service error';
        }
        Common::dieWithJSONP($retMsg);
    }

    /**
     * 获取数据库信息
     */
    public   function  get_mysql_stat() {
        $this->load->database();
        $ret = Common::transresult($this->db->query('SHOW STATUS LIKE "%threads_connected%" '));
        $connected_count     = $ret[0]["Value"];
        $ret =  Common::transresult($this->db->query('SHOW VARIABLES LIKE "max_connections"'));
        $max_connected_count = $ret[0]["Value"];
        $retMsg->retcode     = 0;
        $retMsg->message     = compact('connected_count', 'max_connected_count');
        Common::dieWithJSONP($retMsg);
    }

    /**
     * 获取测试queue值
     */
    public   function  validate_test_queue() {
        $this->load->database();
        include_once APPPATH . 'da/cache/cachecore.php';
        global $g_cache;
        $test_val        = $_REQUEST['test_val'];
        $g_cache->get_cache('lv_queue_test_val_' . $test_val);
        $retMsg->retcode = 0;
        $retMsg->message = $ret;
        if ($ret == $test_val) {
            $retMsg->retcode = 0;
            $retMsg->message = 'success';
            //删除缓存
            $g_cache->delete_cache('lv_queue_test_val_' . $test_val);
        } else {
            $retMsg->retcode = 1;
            $retMsg->message = "mismatch, $ret!=$test_val";
        }
        Common::dieWithJSONP($retMsg);
    }
    /**
     * Test Queue
     */
    public   function  get_queue_info() {
        $q = new \SaeTaskQueue('');
        if (is_callable(array($q,
            'getQueues'))
        ) {
            $retMsg->retcode = 0;
            $res             = json_decode($q->getQueues());
            $retMsg->message = $res->message;
        } else {
            $retMsg->retcode = 1;
            $retMsg->message = '本地Queue不支持getQueues方法';
        }
        Common::dieWithJSONP($retMsg);
    }
}