<?php

$commonConfig = include(__DIR__ . '/../../../lib/common/config/main.php');
$baseConfig = include('base.php');

//defined('STATIC_PATH') or define('STATIC_PATH', '/assets/dist');
defined('STATIC_PATH') or define('STATIC_PATH', __DIR__ . '/../../../static/src');
defined('JS_PATH') or define('JS_PATH', STATIC_PATH . '/js');

$webConfig = array(
	'components' => array(
		'clientScript' => array(
			'coreScriptUrl' => YII_DEBUG ? '/assets/src/js' : JS_PATH,
		),
	),
	'params' => array(
		'looyu' => 'http://lead.soperson.com/20000209/10039948.js',
		'visitorCorpEnable' => true,
	),
);

$config = CMap::mergeArray($commonConfig, $baseConfig, $webConfig);

return $config;
