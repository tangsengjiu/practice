<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/3/3
 * Time: 17:11
 */
defined('STATIC_VER') or define('STATIC_VER', '20160507001');
return array(
	'name' => 'wukong',
	'basePath' => "../protected",
	'defaultController' => 'user',
	'import' => array(
		'application.components.*',
	),
	'components' => array(
		'clientScript' => array(
			//'coreScriptUrl' => YII_DEBUG ? '/assets/src/js' : '/assets/dist/js',
			'coreScriptUrl' => '/assets/src/js',
			'coreScriptPosition' => CClientScript::POS_END,
			'defaultScriptFilePosition' => CClientScript::POS_END,
			'packages' => array(
				'requirejs' => array(
					'js' => array('require.js', 'config.js?v=' . STATIC_VER),
				),
			),
		),
	),
	'params' => array(
		'accessRules' =>array(
			array(
				'allow',
				'controllers' => array('user'),
				'actions' => array('test', 'home'),
			),
			array(
				'allow',
				'controllers' => array('user', 'gentable'),
			),
		),
	)

);