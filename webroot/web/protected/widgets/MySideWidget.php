<?php
/**
 * Created by PhpStorm.
 * User: soulwu
 * Date: 14/10/29
 * Time: PM4:44
 *
 * @property MyController $controller
 * @property LUserModel $user
 * @property int $current
 */

class MySideWidget extends CWidget
{
	const TAB_LICAI = 0;
	const TAB_PROFILE = 1;
	const TAB_WELFARE = 2;
	const TAB_RELATIVE = 3;
	const TAB_COLLEAGUE = 4;
	const TAB_LCS = 5;
	const TAB_MSG = 6;

	public $user;
	public $current;

	protected $tabs;

	public function init()
	{
		$this->user = LUserService::getLoginUser();
		if (empty($this->tabs))
		{
			$this->tabs = array(
				self::TAB_LICAI => array(
					'show' => true,
					'title' => '我的财富',
					'href' => Yii::app()->createUrl('my/index'),
					'current' => false,
					'blank' => false,
					'class' => '',
				),
				self::TAB_PROFILE => array(
					'show' => true,
					'title' => '我的资料',
					'href' => Yii::app()->createUrl('my/profile'),
					'current' => false,
					'blank' => false,
					'class' => '',
				),
				self::TAB_WELFARE => array(
					'show' => true,
					'title' => '福利专区',
					'href' => Yii::app()->createUrl('my/welfare'),
					'current' => false,
					'blank' => false,
					'class' => '',
				),
				self::TAB_RELATIVE => array(
					'show' => LInviteService::canInvite($this->user),
					'title' => '我的推荐',
					'href' => Yii::app()->createUrl('my/invite'),
					'current' => false,
					'blank' => false,
					'class' => '',
				),
				self::TAB_COLLEAGUE => array(
					'show' => $this->controller->checkColleagueCorp($this->user->corpId),
					'title' => '同事推荐',
					'href' => Yii::app()->createUrl('my/colleague'),
					'current' => false,
					'blank' => false,
					'class' => '',
				),
				/*self::TAB_LCS => array(
					'show' => $this->controller->checkCorpIsNoah($this->user),
					'title' => '合作企业',
					'href' => Yii::app()->params['lcs_url'],
					'current' => false,
					'blank' => true,
					'class' => '',
				),*/
				self::TAB_MSG => array(
					'show' => true,
					'title' => '我的消息',
					'href' => Yii::app()->createUrl('my/message'),
					'current' => false,
					'blank' => false,
					'class' => '',
				),
			);
		}

		if (array_key_exists($this->current, $this->tabs))
		{
			$this->tabs[$this->current]['current'] = true;
		}
	}

	public function run()
	{
		$code = '';
		if ($this->user->corpCodeId)
		{
			$corpCode = LCorpCodeService::getCorpCodeById($this->user->corpCodeId);
			if ($corpCode)
			{
				$code = '（企业码：' . $corpCode->corpCode . '）';
			}
		}


		$this->render('myside', array(
				'tabs' => $this->tabs,
				'user' => $this->user,
				'corpCode' => $code,
				'greetings' => $this->getGreeting($this->user->name),
				'colleagueNum' => LUserService::getCorpUserCount($this->user->corpId) - 1,
			));
	}

	private function getGreeting($name)
	{
		$morningTime = strtotime(date("Y-m-d 04:00:00"));
		$afternoonTime = strtotime(date("Y-m-d 12:00:00"));
		$nightTime = strtotime(date("Y-m-d 18:00:00"));

		$curTime = time();
		if ($curTime >= $morningTime && $curTime <= $afternoonTime)
		{
			$greetingWords =  "早上好";
		}
		else if ($curTime >= $afternoonTime && $curTime <= $nightTime)
		{
			$greetingWords =  "下午好";
		}
		else
		{
			$greetingWords =  "晚上好";
		}

		return $name ? $greetingWords."， <strong>$name</strong>" : $greetingWords;
	}
}