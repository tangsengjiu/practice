<?php
/**
 * @var LUserModel $user
 */
?>
<div class="leftbar">
	<div class="user_info">
		<div class="user_name">
			<span><?= $greetings; ?></span>
		</div>
		<div class="user_state">
			<dl>
				<dd><a class="icon_phone act" href="<?= Yii::app()->createUrl('profile/mobile'); ?>">修改手机号</a></dd>
			<?php
			if (!$user->emailVerified)
			{
			?>
				<dd><a class="icon_mail" href="<?= Yii::app()->createUrl('profile/email'); ?>">认证邮箱</a></dd>
			<?php
			}
			else
			{
			?>
				<dd><a class="icon_mail act">邮箱已认证</a></dd>
			<?php
			}
			if (!$user->transactionPasswd)
			{
			?>
				<dd><a class="icon_password" href="<?= Yii::app()->createUrl('profile/auth', array('returnUrl' => Yii::app()->request->getUrl())); ?>">设置交易密码</a></dd>
			<?php
			}
			else
			{
			?>
				<dd><a class="icon_password act" href="<?= Yii::app()->createUrl('profile/transpwd'); ?>">修改交易密码</a></dd>
			<?php
			}
			if (!LBankcardService::hasUserBankcard($user->uid))
			{
			?>
				<dd><a class="icon_card" href="<?= Yii::app()->createUrl('profile/auth', array('returnUrl' => Yii::app()->request->getUrl())); ?>">绑定银行卡</a></dd>
			<?php
			}
			else
			{
			?>
				<dd><a class="icon_card act">银行卡已绑定</a></dd>
			<?php
			}
			?>
			</dl>
		</div>
	</div>
	<ul>
	<?php
	foreach ($tabs as $key => $tab)
	{
		if ($tab['show'])
		{
	?>
		<li><a class="<?= $tab['class'] ? $tab['class'] : ''; ?> <?= $tab['current'] ? 'current' : ''; ?>" href="<?= $tab['href']; ?>"<?= $tab['blank'] ? ' target="_blank"' : ''; ?>><span></span><?= $tab['title'] ?><?= $key == MySideWidget::TAB_MSG ? '<i class="gf_tips J_msg_count" style="display: none;" id="unread">0</i>' : ''; ?></a></li>
	<?php
		}
	}
	?>
	</ul>
</div>
<div class="user_login_info">
<?php
if ($user->lastLoginTime)
{
?>
	<div class="fr">
		<span>上次登录时间</span> <span><?= date("Y-m-d H:i:s", $user->lastLoginTime); ?></span>
	</div>
<?php
}
?>
<?php
if ($user->corpId)
{
?>
	<span><?php
		if ($corpCode)
		{
			echo LCorpService::getInstance()->getCorpNameById($user->corpId) . $corpCode;
		}
		?></span>您有<span><?= $colleagueNum; ?></span>个同事在员工宝理财
<?php
}
?>
</div>