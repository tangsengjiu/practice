<?php
/**
 * Created by PhpStorm.
 * User: liujing
 * Date: 2016/3/6
 * Time: 19:58
 * @property string $jsMain
 * @property string $cssFile
 */

abstract class BaseController extends CController
{
    protected $_jsMain = null;
    protected $cssFile = "global";
    protected $_needJs = false;

    public function getJsMain()
    {
        return $this->_jsMain;
    }

    public function setJsMain($name)
    {
        if ($name)
        {
            $this->_jsMain = "main/$name";
            $this->_needJs = true;
        }
    }

    public function accessRules()
    {
        return Yii::app()->params->accessRules;
    }

    public function getJsDir()
    {
        return YII_DEBUG ? '/assets/src/js' : JS_PATH;
    }

	public function getAssetsDir()
	{
		return YII_DEBUG ? '/assets/src' : STATIC_PATH;
	}

	public function getRequireConfig()
	{
		$urlArgs = 'v=' . STATIC_VER;
		$baseUrl = $this->getJsDir();
		return <<<config
var require = {
	urlArgs: '$urlArgs',
	baseUrl: '$baseUrl',
	waitSeconds: 5
};
config;
	}

}