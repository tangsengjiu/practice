<?php

/**
 * SiteController is the default controller to handle user requests.
 */
class SiteController extends CController
{
	/**
	 * Index action is the default action in a controller.
	 */
	public function actionIndex()
	{
		$value  =  $this->redisTest();
		if ($value)
		{
			echo $value;
		}
		else
		{
			echo 'Hello World11';
		}
	}

	public function redisTest(){
		$redis = new CRedisCache();
		$redis->set("testing", "testing", 20);
		$value = $redis->get("testing");
		return $value;
	}
}