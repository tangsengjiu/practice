<?php

// include Yii bootstrap file
defined('YII_ENV') or define('YII_ENV', 'dev');
defined('YII_DEBUG') or define('YII_DEBUG', true);
//print(dirname(__FILE__).'/../framework/yii.php');//dirname(__FILE__).
require_once(dirname(__FILE__) . '/../../lib/framework/yii.php');
$config = dirname(__FILE__) . '/../protected/config/main.php';
// create a Web application instance and run
Yii::createWebApplication($config)->run();

