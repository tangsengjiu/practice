(function() {
	// 上报js错误
	window.onerror = function(msg, url, line, column, errorObj) {
		var REPORT_URL = '/site/report?type=jserr&err=';
		var m = [msg, url, line, column, window.navigator.userAgent];
		var reportUrl = REPORT_URL + encodeURIComponent(m.join('|'));

		var img = new Image();
		img.onload = img.onerror = function() {
			img = null;
		};
		img.src = reportUrl;
	};
})();