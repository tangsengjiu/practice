requirejs.config({
	baseUrl: '/assets/src/js/',
	shim: {
		'jquery.colorize': {
			deps: ['jquery'],
			exports: 'jQuery.fn.colorize'
		},
		'jquery.scroll': {
			deps: ['jquery'],
			exports: 'jQuery.fn.scroll'
		},
		'backbone.layoutmanager': {
			deps: ['backbone'],
			exports: 'Backbone.LayoutManager'
		}
	},
	paths: {
		'jquery': "lib/jquery-2.2.3.min",
		'underscore': "lib/underscore"
		//'bootstrap': "lib/bootstrap.min"
	}
});