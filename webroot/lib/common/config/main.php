<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/3/21
 * Time: 15:49
 */
$baseConfig = include('base.php');
$commonConfig = array(
	'components' =>array(
		'testDb' => array(
			'connectionString' => 'mysql:host=127.0.0.1;dbname=test',
			'username' => 'root',
			'password' => 'weilver2015',
			'charset' => 'utf8',
		),
		'changeDb' => array(
			'connectionString' => 'mysql:host=127.0.0.1;dbname=change_place',
			'username' => 'root',
			'password' => 'weilver2015',
			'charset' => 'utf8',
		),
		'rabbitDb' => array(
			'host' =>'localhost',
			'port' => 5672,
			'login' => 'guest',
			'password' => 'guest',
		),
	),
);
return CMap::mergeArray($baseConfig, $commonConfig);