<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/3/21
 * Time: 15:49
 */

return array(
	'aliases' => array(
		'common' => dirname(__DIR__),
		'extensions' => dirname(dirname(__DIR__)) . '/extensions',
	),
	'preload' => array(
		'log',
		'preloader',
	),
	'import' => array(
		'common.models.*' ,
		'common.service.*',
		'common.lib.rabbitMq.*',
	),
	'components' => array(
		'urlManager' => array(
			'urlFormat' => 'path',
			'showScriptName' => false,
			'appendParams' => false,
		),
		'testDb' => array(
            'class' => 'CDbConnection',
            'charset' => 'utf8',
        ),
		'changeDb' => array(
			'class' => 'CDbConnection',
			'charset' => 'utf8',
		),
	),
);