<?php

require_once __DIR__ . '/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;
//$connection = new AMQPConnection('127.0.0.1', 5672, 'guest', 'guest');
//$channel = $connection->channel();
//
//$channel->queue_declare('test_queue', true, false, false, false);
//
//echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";

//$callback = function($msg){
//  echo " [x] Received ", $msg->body, "\n";
//  sleep(substr_count($msg->body, '.'));
//  echo " [x] Done", "\n";
//  $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
//};
//
//$channel->basic_qos(null, 1, null);
//$channel->basic_consume('test_queue', '', false, false, false, false, $callback);
//
//while(count($channel->callbacks)) {
//    $channel->wait();
//}
//
//$channel->close();
//$connection->close();
$exchange = 'router';
$queue = 'msgs';

/**
 * 生产者消费者一样的代码
 */

$conn = new AMQPConnection('127.0.0.1', 5672, 'guest', 'guest');
$ch = $conn->channel();

$ch->queue_declare($queue, false, true, false, false);

$ch->exchange_declare($exchange, 'direct', false, true, false);

$ch->queue_bind($queue, $exchange);

/**
 * 发布消息到消息队列
 */

$msg_body = "hello consumer";
echo "11111133333";

$msg = new AMQPMessage($msg_body, array('content_type' => 'text/plain', 'delivery_mode' => 2));
$ch->basic_publish($msg, $exchange);

$ch->close();
$conn->close();

?>