<?php

require_once __DIR__ . '/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

////创建MQ连接
//$connection = new AMQPConnection('127.0.0.1', 5672, 'guest', 'guest');
//$channel = $connection->channel();
////声明队列
//$channel->queue_declare('test_queue', true, false, false, false);
////定义要发送消息
//$msg = new AMQPMessage('Hello json or xml data!');
////发送消息
//$channel->basic_publish($msg, '', 'test_queue');
//
//echo " [x] Sent 'Hello World!'\n";
//
//$channel->close();
//$connection->close();


$exchange = 'router';
$queue = 'msgs';
$consumer_tag = 'consumer';

/**
 * 生产者消费者一样的代码
 */

$conn = new AMQPConnection('127.0.0.1', 5672, 'guest', 'guest');
$ch = $conn->channel();

$ch->queue_declare($queue, false, true, false, false);

$ch->exchange_declare($exchange, 'direct', false, true, false);

$ch->queue_bind($queue, $exchange);


/**
 * 注册消息回调函数
 */

function process_message($msg) {

	echo "receive message : " . $msg->body . "\n";

	$msg->delivery_info['channel']->
	basic_ack($msg->delivery_info['delivery_tag']);

}
$ch->basic_consume($queue, $consumer_tag, false, false, false, false, 'process_message');

/**
 * 关闭连接回调
 */
function shutdown($ch, $conn){
	$ch->close();
	$conn->close();
}
register_shutdown_function('shutdown', $ch, $conn);

/**
 * 循环等待消息
 */

while (count($ch->callbacks)) {
	$ch->wait();
}

?>