<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/3/23
 * Time: 16:17
 */
require_once __DIR__ . '/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;
class AMQPTransport
{
	const WRITE_MODE = 1;
	const READ_MODE = 2;
	protected $exchangeName;
	protected $routingKey;
	protected $queueName;

	public function _construct($exchangeName, $queueName)
	{
		$this->exchangeName = $exchangeName;
		$this->queueName = $queueName;
	}
	public function  consumeMsgFromMq()
	{
		/**
		 * 生产者消费者一样的代码
		 */
		var_dump("testing consumer program");
		$conn = new AMQPConnection('127.0.0.1', 5672, 'guest', 'guest');
		$ch = $conn->channel();
		$ch->queue_declare($this->queueName, false, true, false, false);
		$ch->exchange_declare($this->exchangeName, 'direct', false, true, false);
		$ch->queue_bind($this->queueName, $this->exchangeName);
		$ch->basic_consume($this->queueName, "", false, false, false, false, 'process_message');
		register_shutdown_function('shutdown', $ch, $conn);
		while (count($ch->callbacks)) {
			$ch->wait();
		}
	}
	/**
	 * 注册消息回调函数
	 */

	/**
	 * @param $ch
	 * @param $conn
	 */
	function shutdown($ch, $conn)
	{
		$ch->close();
		$conn->close();
	}

	function process_message($msg)
	{
		var_dump("11111111111");
		echo "receive message : " . $msg->body . "\n";
		$sql = "insert into lj_test(`name`) values('".$msg->body."');";
		/** @var CDbConnection $db */
		$db = Yii::app()->changeDb;
		$db->createCommand($sql)->execute();
		LTablePoolService::createTabelBySql();
		$msg->delivery_info['channel']->
		basic_ack($msg->delivery_info['delivery_tag']);

	}

	public static function  consumeMsg($exchangeName, $queueName)
	{
		var_dump("testing consumer program");
		$conn = new AMQPConnection('127.0.0.1', 5672, 'guest', 'guest');
		$ch = $conn->channel();
		$ch->queue_declare($queueName, false, true, false, false);
		$ch->exchange_declare($exchangeName, 'direct', false, true, false);
		$ch->queue_bind($queueName, $exchangeName);
		$callBack = function($msg)
		{
			$sql = "insert into lj_test(`name`) values('".$msg->body."');";
			/** @var CDbConnection $db */
			$db = Yii::app()->changeDb;
			$db->createCommand($sql)->execute();
			$msg->delivery_info['channel']->
			basic_ack($msg->delivery_info['delivery_tag']);
		};
		$ch->basic_consume($queueName, "", false, true, false, false, $callBack);
		$ch->close();
		$conn->close();
		var_dump("ending~~~");
//		register_shutdown_function('shutdown', $ch, $conn);
//		while (count($ch->callbacks)) {
//			$ch->wait();
//		}
	}

	public static function consumerMsg($exchangeName, $queueName, $consumer_tag)
	{
//连接RabbitMQ
		$conn = new AMQPConnection('127.0.0.1', 5672, 'guest', 'guest');
		$conn->connect();
//设置queue名称，使用exchange，绑定routingkey
		$channel = new AMQPChannel($conn);
		$q = new AMQPQueue($channel);
		$q->setName($queueName);
		$q->setFlags(AMQP_DURABLE);
		$q->declare();
		$q->bind('exchange',$exchangeName);
//消息获取
		$messages = $q->get(AMQP_AUTOACK) ;
		if ($messages){
			var_dump(json_decode($messages->getBody(), true ));
		}
		$conn->disconnect();
	}

	public static function AMPQTransport($exchangeName, $queueName, $msg_body, $defaultMode = self::WRITE_MODE)
	{
		$conn = new AMQPConnection('127.0.0.1', 5672, 'guest', 'guest');
		$ch = $conn->channel();
		$ch->queue_declare($queueName, false, true, false, false);
		$ch->exchange_declare($exchangeName, 'direct', false, true, false);
		$ch->queue_bind($queueName, $exchangeName);
		/**
		 * 发布消息到消息队列
		 */
		$msg = new AMQPMessage($msg_body, array('content_type' => 'text/plain', 'delivery_mode' => 2));
		$ch->basic_publish($msg, $exchangeName);
		$ch->close();
		$conn->close();
	}

}