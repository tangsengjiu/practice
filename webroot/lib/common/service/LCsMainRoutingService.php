<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/3/30
 * Time: 10:53
 */
class LCsMainRoutingService
{
	/**
	 * @param $mrid
	 * @return LCsRoutingModel | null
	 */
	public static function getMainRoutingByMrid($mrid)
	{
		$criteria = new CDbCriteria();
		$criteria->addCondition("mrid =:mrid");
		$criteria->params[":mrid"] = $mrid;
		return LCsMainRoutingModel::model()->find($criteria);
	}
}