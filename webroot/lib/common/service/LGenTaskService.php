<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/3/29
 * Time: 9:44
 */
class LGenTaskService
{
	public static function getCombinedDBTask($routingID, $today, $info)
	{
		$arrTask = array();
		$routing = LCsRoutingService::getRoutingByRid($routingID);
		if (!empty($routing))
		{
			$mainRouting = LCsMainRoutingService::getMainRoutingByMrid($routing->mrID);
			if(empty($mainRouting))
			{
				return null;
			}
			self::dealAllsegmentsOfRouting($mainRouting, $mainRouting->mrid, $routing); // 给每一个航段附上相应的出发日期的间隔的区间值
			foreach($routing->segmentUnitList as $segmentUnit)
			{
				if (count($segmentUnit->segmentList) == 1)
				{ // 单段任务的生成
					self::genSingleSegmentTask($arrTask, $segmentUnit, $mainRouting, $routingID);
				}
				else if (count($segmentUnit->segmentList) == 2)
				{
					self::generateRTTasksForSegments($arrTask, $segmentUnit, $mainRouting, $routingID); // >=2段任务的生成
				}
				else // 大于两段任务的生成
				{
					self::generateTaskForMultiSegments($arrTask, $segmentUnit, $mainRouting, $routingID);
				}
			}
		}
		// 如果该航路是测试航路，将任务状态置为T
		if($routing->status == 'T')
		{
			foreach($arrTask as $task)
			{
				$task->status = 'T';
			}
		}
		self::addTasks($arrTask);
		return $arrTask;
	}

	private function  getMultiArr($segmentUnit)
	{
		$segments = $segmentUnit->segmentList;
		$totalArr = array();
		for($i = 1;$i < count($segments); $i++)
		{//从第二段开始算起
			$tmp = array();
			for($j=$segments[$i]->intervalOne;$j<=$segments[$i]->intervalTwo;$j++)
			{
				array_push($tmp, $j);
			}
			array_push($totalArr, $tmp);
		}//获得所有航段的停留二维天数的数组（每个元素的个数是不确定的）
		return $totalArr;
	}

	/**
	 * 多段任务生成
	 * @param $arrTask
	 * @param $segmentUnit
	 * @param $mainRouting
	 * @param $routingID
	 * @return mixed
	 */
	private function  generateTaskForMultiSegments($arrTask, $segmentUnit, $mainRouting, $routingID)
	{

		$totalArr = $this->getMultiArr($segmentUnit);
//		$retArr = RoutingDA::getRoutingConfig($segmentUnit->suDescription);
		foreach($segmentUnit->channelList as $channel)
		{
			$exp_inters = "";
//			if(count($retArr)>0 && strpos(",".$retArr[0]["channels"], $channel->code)>0)
//			{//存在渠道
//				$exp_inters = $retArr[0]["config"];
//			}
			$arr_inter = array();
			if($exp_inters!=""){
				$arr_inter = explode('/', $exp_inters);
			}
//			$subTask = array();
			$routingChild = $segmentUnit->suDescription;
			$step = $channel->intervalStep;
//			$intervalFrom = $mainRouting->intervalFrom;
//			$intervalTo = $mainRouting->intervalTo;
			$departDayFrom = $channel->departDayFrom;
			$departDayTo = $channel->departDayTo;
			$today     = date('Y-m-d'); //today
			$config = LCsPuService::getRoutingConfig();
//		if($departDayTo==null || $departDayFrom==null || $departDayTo<=$departDayFrom || $step==null || $step<=0 ){
//			continue;
//		}
			for($addday = $departDayFrom; $addday <= $departDayTo + $departDayFrom; $addday += $step)
			{
				if (count($arr_inter) > 0)
				{ // 生成多余的时间间隔项
					for($p = 0; $p < count($arr_inter); $p++)
					{
						$ddateArr = explode(' ', LCommonService::DateAdd('d', $addday, $today));
						$task = new stdClass();
						$task = self::TaskCopy($task, $channel, $arr_inter[$p], $mainRouting, $routingChild, $routingID, $segmentUnit, $ddateArr);
						$this->transRouting($task, $config);
						array_push($arrTask, $task);
					}
				}
				else
				{
					$ddateArr = explode(' ', LCommonService::DateAdd('d', $addday, $today));
					$task                = new stdClass();
					$task = self::TaskCopy($task, $channel, 0, $mainRouting, $routingChild, $routingID, $segmentUnit, $ddateArr);
					$ret = array();
					$timeCom =array();
					$this->getInterval($totalArr, 1, $totalArr[0], $totalArr[1], $timeCom, $task, $arrTask);
				}
			}
		}
		return $arrTask;
	}

	private function  getInterval($all, $pos, $first, $second, $ret, $task ,&$subTask1)
	{
		$allNums = 0;
		$ret1 = array();
		$ret = $ret1;
		for($i=0;$i<count($first);$i++)
		{
			for($j=0;$j<count($second);$j++)
			{
				$tmp = $first[$i].",".$second[$j];
				array_push($ret,$tmp);
			}
		}
		$pos = $pos+1;
		if($pos<=count($all)-1)
		{
			getInterval($all, $pos, $ret, $all[$pos], $ret, $task, $subTask1);
		}
		else
		{//
			for($m=0;$m<count($ret);$m++)
			{
				$taskNew = clone  $task;
				$taskNew->interval = $ret[$m];
				array_push($subTask1, $taskNew);
			}
		}
		return $subTask1; // 第一个出发日期定了后，剩下的航段的所有的时间组合
	}

	/**
	 * 生成往返任务的处理
	 * @param $arrTask
	 * @param $segmentUnit
	 * @param $mainRouting
	 * @param $routingID
	 * @return array
	 */
	private function generateRTTasksForSegments(&$arrTask, $segmentUnit, $mainRouting, $routingID)
	{
//		$retArr = RoutingDA::getRoutingConfig($segmentUnit->suDescription);
		if (count($segmentUnit->segmentList) == 2)
		{
			foreach($segmentUnit->channelList as $channel)
			{
				$exp_inters = "";
//				if (count($retArr) > 0 && strpos(",".$retArr[0]["channels"], $channel->code) > 0)
//				{//存在渠道
//					$exp_inters = $retArr[0]["config"];
//				}
				$arr_inter = array();
				if ($exp_inters != "")
				{
					$arr_inter = explode('/', $exp_inters);
				}
				$routingChild = $segmentUnit->suDescription;
				$step = $channel->intervalStep;
				$departDayFrom = $channel->departDayFrom;
				$departDayTo = $channel->departDayTo;
				$today = date('Y-m-d',time()); //today
				$config = LCsPuService::getRoutingConfig();
//			if($departDayTo==null || $departDayFrom==null || $departDayTo<=$departDayFrom || $step==null || $step<=0 ){
//				continue;
//			}
				$flag1 = false;
				$flag2 = false;
				if (strpos($channel->rtstatus,'R') !== false && strpos($routingChild, '//') === false){
					$flag1 = true;
				}
				if (strpos($channel->rtstatus,'G') !== false && strpos($routingChild, '//') !== false)
				{
					$flag2 = true;
				}
				if ($flag1 == true || $flag2 == true)
				{ // 若渠道只支持往返
					//if(strpos($channel->rtstatus, 'R')===TRUE || $channel->rtstatus=='G'){//针对往返和缺口的
					for($addDay = $departDayFrom; $addDay <= $departDayTo + $departDayFrom; $addDay += $step)
					{
						$start = $segmentUnit->segmentList[1]->intervalOne;
						$end = $segmentUnit->segmentList[1]->intervalTwo;
						if ($channel->code == "CTRIP")
						{
							$start = 1;
							$end = 7;
						}
						for($k = $start;$k <= $end; $k++)
						{
							$ddateArr = explode(' ', LCommonService::DateAdd('d', $addDay, $today));
							$task                = new stdClass();
							$task = self::TaskCopy($task, $channel, $k, $mainRouting, $routingChild, $routingID, $segmentUnit, $ddateArr);
							self::transRouting($task, $config);
							array_push($arrTask, $task);
						}
						if(count($arr_inter)>0)
						{ // 生成多余的时间间隔项
							for($p = 0;$p < count($arr_inter); $p++)
							{
								$ddateArr = explode(' ', LCommonService::DateAdd('d', $addDay, $today));
								$task                = new stdClass();
								$task = self::TaskCopy($task, $channel, $arr_inter[$p], $mainRouting, $routingChild, $routingID, $segmentUnit,
$ddateArr);
								self::transRouting($task, $config);
								array_push($arrTask, $task);
							}
						}
					}
				}
				else
				{
					if($channel->rtstatus == 'S')
					{//只支持单程的
						$segs = explode("-", $routingChild);
						if (count($segs) == 3 && strpos($routingChild, '//') === false)
						{ // 往返的行程，可以生成单程的任务，缺口的忽略
							$subtask = self::taskCommon($segmentUnit, $mainRouting, $channel, $routingID, 1);
							$arrTask = array_merge($arrTask, $subtask);
						}
					}
				}
			}
		}
		return $arrTask;
	}

	/**
	 * @param $arrTask
	 * @param $SegUnit
	 * @param $mainRouting
	 * @param $routingID
	 */
	private function genSingleSegmentTask(&$arrTask, $SegUnit, $mainRouting, $routingID)
	{
		foreach($SegUnit->channelList as $channel)
		{
			if ($channel->rtstatus == 'R')
			{//如果只支持单程任务的生成
				continue;
			}
			else
			{
				$subtask = self::taskCommon($SegUnit, $mainRouting, $channel, $routingID, 0);
				$arrTask = array_merge($arrTask, $subtask);
			}
		}

	}

	/**
	 * 抽取生成任务的函数
	 * @param $SegUnit
	 * @param $mainRouting
	 * @param $channel
	 * @param $routingID
	 * @param $rtValue
	 * @return array
	 */
	private function taskCommon($SegUnit, $mainRouting, $channel, $routingID, $rtValue)
	{
		$subTask = array();
		$routingChild = $SegUnit->suDescription;
		$step = $channel->intervalStep;
//		$intervalFrom = $mainRouting->intervalFrom;
//		$intervalTo = $mainRouting->intervalTo;
		$departDayFrom = $channel->departDayFrom;
		$departDayTo = $channel->departDayTo;
		$today     = date('Y-m-d'); //today
		$config = LCsPuService::getRoutingConfig();
// 	if($departDayTo==null || $departDayFrom==null || $departDayTo<=$departDayFrom || $step==null || $step<=0 ){
// 		continue;
// 	}
		for($addday = $departDayFrom; $addday <= $departDayTo + $departDayFrom; $addday += $step)
		{
			$ddateArr = explode(' ', LCommonService::DateAdd('d', $addday, $today));
			$task = new stdClass();
			$task = self::TaskCopy($task, $channel, 0 ,$mainRouting, $routingChild, $routingID, $SegUnit, $ddateArr);
			$rt = explode("-", $routingChild);
			if ($rtValue == 1)
			{
				if (count($rt) == 3)
				{
					$task->routing = $rt[0].'-'.$rt[1];
					self::transRouting($task, $config);
					array_push($subTask, $task);
					$tasknew = clone  $task;
					$tasknew->routing = $rt[1].'-'.$rt[0];
					self::transRouting($tasknew,$config);
					array_push($subTask, $tasknew);
				}
			}
			else
			{
				self::transRouting($task, $config);
				array_push($subTask, $task);
			}
		}
		return $subTask;
	}


	private function TaskCopy($task, $channel, $k, $mainRouting, $routingChild, $routingID, $segmentUnit, $ddateArr)
	{
		$task->status = 'W';
		$task->channelcode = $channel->code;
		$task->departdate = $ddateArr[0];
		$task->interval = $k;
		$task->mrid = $mainRouting->mrid;
		$task->routing = $routingChild;
		$task->routingid = $routingID;
		$task->suid = $segmentUnit->suID;
		return $task;
	}


	/**
	 * 将任务中的路由字段转换成标准的三字码形式
	 * Enter description here ...
	 * @param unknown_type $tasknew
	 * @param unknown_type $config
	 */
	private function transRouting(&$tasknew, $config)
	{
		$routing_new = self::RoutingConvert($tasknew->routing, $config, $tasknew->channelcode);
		if ($routing_new != null)
		{
			$tasknew->changedrouting = $routing_new;
		}
		else
		{
			$tasknew->changedrouting = null;
		}
	}

	/**
	 * 转换路由中的三字码
	 * @param $taskRouting
	 * @param $config
	 * @param $channelCode
	 * @return null|string
	 */
	private function RoutingConvert($taskRouting, $config, $channelCode)
	{
		$routing_new = "";
		$routings = explode('-',$taskRouting);
		for($i1 = 0; $i1 < count($routings) ; $i1++)
		{
			for($i2 = 0; $i2 < count($config); $i2++)
			{
				if ( $channelCode == $config[$i2]["channelCode"] && $routings[$i1] == $config[$i2]["oriCode"])
				{
					$routings[$i1] = $config[$i2]["channelCode"];
				}
			}
		}
		for($i3 = 0; $i3<count($routings)-1;$i3++)
		{
			$routing_new .= $routings[$i3]."-";
		}
		$routing_new .= $routings[count($routings)-1];
		if ($taskRouting != $routing_new)
			return $routing_new;
		else
			return null;
	}


	/**
	 * 对一个路由上的所有的航段进行处理
	 * @param $mainRouting
	 * @param $mRouting
	 * @param $routing
	 * @return array
	 */
	private function dealAllSegmentsOfRouting($mainRouting, $mRouting, &$routing)
	{
		$allSegments = array();
		foreach($routing->segmentUnitList as $segmentUnit)
		{//获取与一个路由相关的所有的航段
			foreach($segmentUnit -> segmentList as $segment)
			{
				array_push($allSegments, $segment);
			}
		}
//		$this->sortObjArrayBySegNo($mainRouting, $mRouting, $allSegments);
		//	 $allSegments = sortObjArrayBysegNo(&$allSegments);
		return $allSegments;
	}

	/**
	 * 将对象数组根据segmentNo属性进行排序
	 * @param $mainRouting
	 * @param $MRouting
	 * @param $segments
	 * @return mixed
	 */
	private function sortObjArrayBySegNo($mainRouting, $MRouting, &$segments)
	{
		$segments = $this->uSortSegment($segments);
		$this->asureSegmentIntervals($mainRouting, $MRouting, $segments);
		return $segments;
	}


	/**
	 * 排序函数
	 * @param $arr
	 * @return mixed
	 */
	private function uSortSegment($arr)
	{
		usort($arr, 'SegmentNOCmp');
		return $arr;
	}

	/**
	 * 确定每一段的出发和到达之间的间隔
	 * @param $mainRouting
	 * @param $MRouting
	 * @param $segments
	 */
	private function asureSegmentIntervals($mainRouting,$MRouting,&$segments)
	{
		$inter = 0;
		if(count(explode(",",$MRouting))>1)
		{
			$inter = 4;
		}
		else
		{
			$inter = 7;
		}
		if (count($segments) >= 2)
		{
			$segments[0]->intervalOne = 0;
			$segments[0]->intervalTwo = 0;
			for($i = 1; $i < count($segments); $i++)
			{
				if($segments[$i]->isDepartTransfer == 0)
				{
					$segments[$i]->intervalOne = $mainRouting->intervalFrom+$segments[$i-1]->intervalOne;
					$segments[$i]->intervalTwo = $mainRouting->intervalTo+$segments[$i-1]->intervalTwo;
				}
				elseif ($segments[$i]->isDepartTransfer == 1)
				{
					$segments[$i]->intervalOne = 0 + $segments[$i-1]->intervalOne;
					$segments[$i]->intervalTwo = 1 + $segments[$i-1]->intervalTwo;
				}
				else // 分成两种情况，前后段缺和间隔段缺
				{
					if ($segments[$i]->isDepartTransfer == 2)
					{//缺口且非转机城市
						if ($segments[$i]->departCity != $segments[$i-1]->arriveCity )
						{//前后段缺口//$segments[$i]->suID==$segments[$i-1]->suID ||
							$segments[$i]->intervalOne = 2 * $mainRouting->intervalFrom + $segments[$i-1]->intervalOne;
							$segments[$i]->intervalTwo = 2 * $mainRouting->intervalTo + $segments[$i-1]->intervalTwo;
						}
						else // 间隔段缺口，按正常的逻辑走
						{
							$segments[$i]->intervalOne = $mainRouting->intervalFrom + $segments[$i-1]->intervalOne;
							$segments[$i]->intervalTwo = $mainRouting->intervalTo + $segments[$i-1]->intervalTwo;
						}
					}
					else // $segments[$i]->isDepartTransfer==3缺口并且是转机城市
					{
						if ($segments[$i]->departCity!=$segments[$i-1]->arriveCity )
						{ // 前后段缺口//$segments[$i]->suID==$segments[$i-1]->suID ||
							$segments[$i]->intervalOne = $mainRouting->intervalFrom + $segments[$i-1]->intervalOne+0;
							$segments[$i]->intervalTwo = $mainRouting->intervalTo + $segments[$i-1]->intervalTwo+1;
						}
						else // 间隔段缺口，按正常的逻辑走
						{
							$segments[$i]->intervalOne = $segments[$i-1]->intervalOne+0;
							$segments[$i]->intervalTwo = $segments[$i-1]->intervalTwo+1;
						}
					}
				}
			}
		}
	}

	public static function addTasks($arrTask)
	{
		/** @var CDbConnection $db */
		$db = Yii::app()->changeDb;
		if (empty($arrTask))
		{
			return array();
		}
		try
		{
			$headSql=
				"INSERT INTO task
				(
				channelcode,
				`status`,
				departDate,
				`interval`,
				routing,lastupdatetime,routingid,mrid,suid,realCrawlRouting,hash,createtime
				)
				VALUES
				";

			$valueTpl = "(':channelcode',':status',':departdate',':interval',':routing',':lastupdatetime',:routingid,:mrid,:suid,:real_crawlrouting,':hash',':createtime')";

			$sql = $headSql;
			$batchCount = 0;
			for($i=0;$i<count($arrTask);$i++)
			{
				$value = $valueTpl;
				$value =  str_replace(':channelcode', $arrTask[$i]->channelcode, $value);
				$value =  str_replace(':status', $arrTask[$i]->status, $value);
				$value =  str_replace(':departdate', $arrTask[$i]->departdate, $value);
				$value =  str_replace(':interval', $arrTask[$i]->interval, $value);
				$value =  str_replace(':routingid',$arrTask[$i]->routingid, $value);
				$value =  str_replace(':routing', $arrTask[$i]->routing, $value);
				$value =  str_replace(':mrid', $arrTask[$i]->mrid, $value);
				$value =  str_replace(':lastupdatetime', date('Y-m-d H:i:s'), $value);
				$value =  str_replace(':suid',$arrTask[$i]->suid, $value);

				$value =  str_replace(':real_crawlrouting', "'".$arrTask[$i]->changedrouting."'", $value);

				$hash =  $arrTask[$i]->departdate."_".$arrTask[$i]->channelcode."_".$arrTask[$i]->interval."_".$arrTask[$i]->routing;
				$value =  str_replace(':hash',$hash, $value);
				$createtime = date("Y-m-d", strtotime(date("Y-m-d")) + 24*60*60) . " 01:00";	// 创建时间伪造为第二天
				$value =  str_replace(':createtime',$createtime, $value);

				$sql .=$value;
				$sql.=',';
				$batchCount++;

				if ($batchCount == 20 || $i == count($arrTask) - 1)
				{
					$sql = substr($sql, 0, strlen($sql)-1);
					$db->createCommand($sql)->execute();
					$sql = $headSql;
					$batchCount =0;
				}
			}
		}
		catch(Exception $e)
		{
			die("gen task wrong");
		}
	}

	public static function genTaskByMRouting()
	{

	}

}