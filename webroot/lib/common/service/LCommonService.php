<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/3/30
 * Time: 12:59
 */
class LCommonService
{
	public static function DateAdd($interval, $number, $date) {
		$date_array = getdate(strtotime($date));
		$hor        = $date_array["hours"];
		$min        = $date_array["minutes"];
		$sec        = $date_array["seconds"];
		$mon        = $date_array["mon"];
		$day        = $date_array["mday"];
		$yar        = $date_array["year"];
		switch ($interval) {
			case "y":
				$yar += $number;
				break;
			case "q":
				$mon += ($number * 3);
				break;
			case "m":
				$mon += $number;
				break;
			case "w":
				$day += ($number * 7);
				break;
			case "d":
				$day += $number;
				break;
			case "h":
				$hor += $number;
				break;
			case "n":
				$min += $number;
				break;
			case "s":
				$sec += $number;
				break;
		}
		return date("Y-m-d H:i:s", mktime($hor, $min, $sec, $mon, $day, $yar));
	}
}