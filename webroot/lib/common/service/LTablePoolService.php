<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/3/22
 * Time: 14:14
 */
class LTablePoolService
{
	public static  function  createTabelBySql()
	{
		/** @var CDbConnection $db */
		$db = Yii::app()->changeDb;
		$sql = "create table if not exists lj_test(
`id` INT(11) NOT NULL  auto_increment comment '自增长ID',
`name` VARCHAR(255) NOT  NULL comment '姓名',
PRIMARY KEY (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8
";
		$db->createCommand($sql)->execute();
	}
	/**
	 * @param $dsn example:mysql:host=127.0.0.1;dbname=test
	 * @param $username
	 * @param $passwd
	 * @return null|CDbConnection conn
	 */
	public static function newConn($dsn, $username, $passwd)
	{
		try
		{
			$conn = new CDbConnection($dsn, $username, $passwd);
			return $conn;
		}
		catch (Exception $e){
			return null;
		}
	}

	/**
	 * @param $dsn
	 * @param $dbName
	 * @param $user
	 * @param $pwd
	 * @return CDbConnection|null
	 */
	public static function getConn($dsn, $dbName, $user, $pwd)
	{
		return self::newConn("mysql:host=" . $dsn . ";dbname=" . $dbName, $user, $pwd);
	}

	public static function genTableByConfig($routing)
	{
		$serverInfo = self::getAllDataBase();
		// table已经存在
		if(!empty($server = self::getFlightTableConfigByRouting($routing)))
		{
			var_dump("table have exist");
		}
		else
		{
			// table 尚未被创建，先检查有没有还有剩余表空间的DB，有的话就直接在相关的DB下创建对应的表，没有就报警
			if(count($serverInfo) > 0)
			{
				$sql = self::getGenTablePoolSql($routing);
				// 创建对应的flight的表格
				$conn = self::getConn($serverInfo[0]["ip"], $serverInfo[0]["dbname"], $serverInfo[0]["user"], $serverInfo[0]["pwd"]);
				$conn->createCommand($sql)->execute();
				$sql = "insert into as_config_ft(routing,sid,dbname)values('$routing',".$serverInfo[0]["sid"].",'".$serverInfo[0]["dbname"]."')";
				/** @var CDbConnection $db */
				$db = Yii::app()->changeDb;
				$db->createCommand($sql)->execute();
			}
			else
			{
				// DB服务器不足报警邮件发出
			}
		}
	}

	function getFlightTableConfigByRouting($routing)
	{
		/** @var CDbConnection $db */
		$db = Yii::app()->changeDb;
		$sql = "select f.dbname,f.routing,s.ip,s.user,s.pwd from as_config_ft f inner join as_server s on f.sid=s.sid and f.routing='.$routing.' limit 1";
		$ret = $db->createCommand($sql)->queryAll();
		return count($ret) > 0 ? $ret[0] : null;
	}
	/**
	 * 获取所有可用有效server中的数据库
	 * @return array|CDbDataReader
	 */
	public static function  getAllDataBase()
	{
		/** @var CDbConnection $db */
		$db = Yii::app()->changeDb;
		try
		{
			$sql = "select d.*,s.* from as_config_db d inner join as_server s on d.sid=s.sid  and  d.tableCount < 100 limit 1";
			$ret = $db->createCommand($sql)->queryAll();
			return $ret;
		}
		catch(Exception $e)
		{
			$sql = "insert into log_err(`type`,content)values('寻找数据库个数没到10个的服务器_ft','".json_encode($e)."')";
			$db->createCommand($sql)->execute();
		}
	}

	public static function getallserverinfo()
	{
		//寻找数据库个数没到10个的服务器
		/** @var CDbConnection $db */
		$db = Yii::app()->changeDb;
		try
		{
			$sql = " SELECT sid,ip,`user`,pwd FROM (SELECT COUNT(DISTINCT cfg.dbname) AS ct,cfg.sid,sev.user,sev.ip,sev.pwd FROM as_config_ft cfg LEFT JOIN as_server sev  ON sev.sid = cfg.sid and sev.status='S' GROUP BY sid) tb WHERE ct < 10";
			$ret = $db->createCommand($sql)->query();
			if (count($ret) > 0 )
			{
				return  $ret;
			}
			if (count($ret)==0)
			{
				$sql = "SELECT sid,ip,`user`,pwd FROM as_server WHERE sid NOT IN (SELECT sid FROM as_config_ft) and `status`='S'";
				$ret =$db->createCommand($sql)->query();
				return $ret;
			}
		}
		catch (Exception $e)
		{
			$sql = "insert into log_err(`type`,content)values('寻找数据库个数没到10个的服务器_ft','".json_encode($e)."')";
			$db->createCommand($sql)->execute();
		}
	}
	static function getdbname()
	{//寻找那些表个数没到100个的数据库信息
		/** @var CDbConnection $db */
		$db = Yii::app()->changeDb;
		try
		{
			$sql = " SELECT * FROM (SELECT COUNT(cfg.routing) AS ct,cfg.dbname,cfg.sid,sev.ip,sev.pwd FROM as_config_ft cfg INNER JOIN as_server sev ON sev.sid = cfg.sid and sev.status='S' GROUP BY dbname,sid) tb WHERE ct<1001";
			$ret = $db->createCommand($sql)->query();
			return $ret;
		}
		catch (Exception $e)
		{
			$sql = "insert into log_err(`type`,content)values('寻找那些表个数没到100个的数据库信息_ft','".json_encode($e)."')";
			$db->createCommand($sql)->execute();
		}
	}
	//检查相关表是否已存在
	/**
	 * @param $routing
	 * @return array
	 */
	static function checkTableExist($routing)
	{
		/** @var CDbConnection $db */
		$db = Yii::app()->changeDb;
		try
		{
			$sql = 'SELECT  sev.ip,sev.user,sev.pwd,cfg.dbname,cfg.routing FROM as_server
		sev INNER JOIN as_config_ft cfg ON sev.sid = cfg.sid WHERE routing=' . '"$routing"';
			$rowCount = $db->createCommand($sql)->query()->rowCount;
			return $rowCount > 0;
		}
		catch (Exception $e)
		{
			$sql = "insert into log_err(`type`,content)values('checktableexist','".json_encode($e)."')";
			$db->createCommand($sql)->execute();
		}
	}
	static function trans_routing($routing)
	{
		$routing_new = str_replace("-", "_", $routing);
		$routing_new  = str_replace("//", "_GAP_", $routing_new);
		return $routing_new;
	}
	static function getallroutingconfigs()
	{//获得所有routing的数据库的配置信息
		/** @var CDbConnection $db */
		$db = Yii::app()->changeDb;
		try
		{
			$sql = Common::prepareSql('SELECT  sev.ip,sev.user,sev.pwd,cfg.dbname,cfg.routing FROM as_server
		sev INNER JOIN as_config_ft cfg ON sev.sid = cfg.sid ');
			$ret  = DataAccess::query($sql, array(),$db);
			$allconfigs = array();
			if(count($ret)>0){
				for($i=0;$i<count($ret);$i++){
					$obj = new stdClass();
					$obj->host = $ret[$i]["ip"];
					$obj->username = $ret[$i]["user"];
					$obj->pwd = $ret[$i]["pwd"];
					$obj->dbname = $ret[$i]["dbname"];
					$obj->dbname = $ret[$i]["dbname"];
					$allconfigs[$ret[$i]["routing"]] = $obj;
				}
			}
			return $allconfigs;
		}
		catch (Exception $e)
		{
			$sql = "insert into log_err(`type`,content)values('获得所有routing的数据库的配置信息','".json_encode($e)."')";
			$db->createCommand($sql)->execute();
		}
	}

	public static  function getGenTablePoolSql($routing)
	{
		$sql = "
CREATE TABLE `ft_$routing` (
	`id` INT(11) NOT NULL AUTO_INCREMENT ,
	`flightno` VARCHAR (150),
	`flight_Routing` VARCHAR (300),
	`datime` VARCHAR (600),
	`inter` VARCHAR (150),
	`crawtime` TIMESTAMP ,
	`result_id` DOUBLE ,
	`modified_time` TIMESTAMP ,
	`price` DECIMAL (20),
	`price_type` VARCHAR (60),
	`currency` VARCHAR (60),
	`source_content` BLOB ,
	`api_type` VARCHAR (30),
	`taskid_spider` DOUBLE ,
	`taskid_savetask` DOUBLE ,
	`datime_full` VARCHAR (1500),
	`dadate` VARCHAR (1500),
	`departdate` VARCHAR (60),
	`interval` VARCHAR (30),
	`channel` VARCHAR (60),
	`routing` VARCHAR (150),
	`hash` VARCHAR (150),
	`tax` DECIMAL (11),
	PRIMARY KEY (`id`),
    UNIQUE KEY `flightno_hash_idx` (flightno,`hash`),
    INDEX flightno2(flightno),
	INDEX hash_idx(`hash`),
	INDEX routing_idx(routing)
) ENGINE=INNODB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

";
		return trim($sql);
	}
	public static function genTableBySql($sql)
	{

	}

}