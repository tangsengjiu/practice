<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/3/21
 * Time: 16:09
 */
class LCsPuService
{
	public static  function getPuByDescription($desc)
	{
		$criteria = new CDbCriteria();
		$criteria->addCondition("pudescription =:pudescription");
		$criteria->params[":pudescription"] = $desc;
		return LCsPuModel::model()->findAll($criteria);
	}

	/**
	 * @return LCsRoutingConfigModel|null
	 */
	public static function getRoutingConfig()
	{
		$criteria = new CDbCriteria();
		$criteria->addCondition("status='S'");
		return LCsRoutingConfigModel::model()->findAll($criteria);
	}
}