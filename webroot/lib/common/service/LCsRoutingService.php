<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/3/29
 * Time: 18:43
 */
class LCsRoutingService
{
	/**
	 * @param $rid
	 * @return LCsSegmentUnitModel[] | null
	 */
	public static function getSegmentUnitByRid($rid)
	{
		$criteria = new CDbCriteria();
		$criteria->addCondition("rid =:rid");
		$criteria->params[":rid"] = $rid;
		return LCsSegmentUnitModel::model()->findAll($criteria);
	}

	/**
	 * @param $suid
	 * @return LCsSegmentModel[] | null
	 */
	public static function getSegmentBySuid($suid)
	{
		$criteria = new CDbCriteria();
		$criteria->addCondition("suid =:suid");
		$criteria->params[":suid"] = $suid;
		$criteria->order = "segmentNO";
		return LCsSegmentModel::model()->findAll($criteria);
	}

	public static function getSegmentChannel($suid)
	{
		$sql = 'SELECT c.* FROM cs_segmentunitchannel uc
							JOIN cs_channel c ON uc.channelCode= c.channelCode and c.IsUsed = 1
							where uc.SUID='.$suid;
		/** @var CDbConnection $db */
		$db = Yii::app()->changeDb;
		return $db->createCommand($sql)->queryAll();
	}

	public static function getRoutingByRid($routingID)
	{

		$criteria = new CDbCriteria();
		$criteria->addCondition('rid =:rid');
		$criteria->params[":rid"] = $routingID;
		$routingModel =  LCsRoutingModel::model()->find($criteria);
		$routing = self::transRoutingModel($routingModel);
		if(!empty($routing))
		{
			$segmentUnits = self::getSegmentUnitByRid($routingID);
			$routing->segmentUnitList = array();
			/** @var  LCsSegmentUnitModel $segUnit */
			foreach ($segmentUnits as $segUnit)
			{
				$segmentUnit = new stdClass();
				$segmentUnit -> suID = $segUnit->suid;
				$segmentUnit -> suDescription = $segUnit->suDescription;
				$segmentUnit -> rID = $segUnit->rid;

				$segments = self::getSegmentBySuid($segUnit->suid);
				$segmentUnit -> segmentList = array();
				foreach ($segments as $seg)
				{
					$segment = new stdClass();
					$segment->sID = $seg->sid;
					$segment->segmentNO = $seg->segmentNO;
					$segment->airlineCode = $seg->airLineCode;
					$segment->departCity = $seg->departCity;
					$segment->arriveCity = $seg->arriveCity;
					$segment->isDepartTransfer = $seg->isDepartTransfer;
					$segment->suID = $seg->suid;
					$segment->intervalOne = 0;
					$segment->intervalTwo = 0;
					array_push($segmentUnit -> segmentList, $segment);
				}
				//add segment ****** end
				//******add channel
				$channels = self::getSegmentChannel($segUnit->suid);
				$segmentUnit -> channelList = array();
				if (!empty($channels))
				{
					foreach ($channels as $chl)
					{
						$channel =new stdClass();
						$channel->cID = $chl['cid'];
						$channel->code = $chl['channelCode'];
						$channel->departDayFrom = $chl['departDayFrom'];
						$channel->departDayTo = $chl['departDayTo'];
						$channel->intervalStep = $chl['intervalStep'];
						$channel->status = $chl['status'];
						$channel->rtstatus = $chl['rtStatus'];//添加往返、单程的说明
						$channel->isCityRouting = $chl['isCityRouting'];
						array_push($segmentUnit -> channelList, $channel);
					}
				}
				//add channel ****** end
				array_push($routing->segmentUnitList, $segmentUnit);
			}
		}
		return $routing;
	}

	/**
	 * @param  LCsRoutingModel $routingModel
	 * @return null|Routing
	 */
	private function transRoutingModel($routingModel)
	{
		if(!empty($routingModel))
		{
			$routing = new stdClass();
			$routing->mrID = $routingModel->mrid;
			$routing->rID = $routingModel->rid;
			$routing->routing = $routingModel->routing;
			$routing->status = $routingModel->status;
			return $routing;
		}
		return null;
	}
}