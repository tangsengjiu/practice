<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/3/29
 * Time: 17:28
 * @property int $rid
 * @property string $routing
 * @property string $routingCheck
 * @property string $status,
 * @property int $mrid
 * @property string $allocator
 * @property dateTime $beginTime
 * @property dateTime $finishTime
 * @property string $allocatedStatus
 * @property string $newRouting
 * @property string $weekday
 * @property string $type
 * @property string $ctripOne
 * @property
 */
class LCsRoutingModel extends CActiveRecord
{
	public function tableName()
	{
		return 'cs_routing';
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->changeDb;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return  the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}