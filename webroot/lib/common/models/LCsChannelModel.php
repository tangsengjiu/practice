<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/3/31
 * Time: 12:43
 */
class LCsChannelModel extends CActiveRecord
{
	public function tableName()
	{
		return 'cs_channel';
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->changeDb;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return  the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}