<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/3/21
 * Time: 15:58
 * @property integer $puid
 * @property string $pudescription
 * @property string $isctrip
 * @property string $isuseful
 * @property string $isexp
 * @property string $ischina
 * @property string $createtime
 * @property string $type
 */
class LCsPuModel extends CActiveRecord
{
	public function tableName()
	{
		return 'cs_pu';
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->testDb;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return  the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}