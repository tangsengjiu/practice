<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/3/29
 * Time: 9:44
 * @property int $id
 * @property string $channelCode
 * @property string $status
 * @property dateTime $createTime
 * @property int $machineId
 * @property string $departDate
 * @property string $interval
 * @property string $routing
 * @property int $orderId
 * @property string $name
 * @property int $routingId
 * @property int $mrid
 * @property int $suid
 * @property string $realCrawlRouting
 * @property string $hash
 */
class LTaskModel extends  CActiveRecord
{
	public function tableName()
	{
		return 'task';
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->changeDb;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return  the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}