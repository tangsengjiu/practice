<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/3/31
 * Time: 11:47
 * @property int $sid
 * @property int $segmentNO
 * @property string $departCity
 * @property string $arriveCity
 * @property string $isDepartTransfer
 * @property int $suid
 * @property string $airLineCode
 * @property int $rid
 * @property $puid
 */
class LCsSegmentModel extends CActiveRecord
{

	public function tableName()
	{
		return 'cs_segment';
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->changeDb;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return  the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}