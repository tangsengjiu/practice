<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/3/30
 * Time: 11:25
 */

class MainRouting
{
	public $mrID;
	public $mroutingID;
	public $intervalFrom;
	public $intervalTo;
	public $weekdays;
	public $saveTasktime;
	public $status;
}

class Routing
{
	public $rID;
	public $routing;
	public $mrID;
	public $status;
	public $segmentUnitList;//array of SegmentUnit
}

class Segment
{
	public $sID;
	public $segmentNO;
	public $departCity;
	public $arriveCity;
	public $isDepartTransfer;
	public $suID;
	public $airlineCode;
}

class SegmentUnit
{
	public $suID;
	public $suNO;
	public $suDescription;
	public $rID;
	public $segmentList; //array of Segment
	public $channelList;//array of SegmentUnitChannel
}
//param for mainrouting list
class MainRoutingParam{
	public $pageNO;
	public $pageSize;
}
//param for routing list
class RoutingParam
{
	public $pageNO;
	public $pageSize;
	public $mRoutingID;
}

class Channel
{
	public $cID;
	public $name;
	public $code;
	public $departDayFrom;
	public $departDayTo;
	public $intervalStep;
	public $status;
	public $isCityRouting;
}