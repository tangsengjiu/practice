<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/3/31
 * Time: 11:32
 * @property int $suid
 * @property string $suDescription
 * @property int $rid
 * @property int $puid
 */
class LCsSegmentUnitModel extends CActiveRecord
{
	public function tableName()
	{
		return 'cs_segmentunit';
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->changeDb;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return  the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}