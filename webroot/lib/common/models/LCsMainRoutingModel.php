<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/3/29
 * Time: 18:53
 * @property int $mrid
 * @property string $mRouting
 * @property int $intervalFrom
 * @property int $intervalTo
 * @property string $weekDays
 * @property string $status
 * @property int $saveTaskMinute
 * @property dateTime $createTime
 * @property dateTime $updateTime
 * @property string $updator
 * @property string creator
 * @property double grade
 */
class LCsMainRoutingModel extends CActiveRecord
{
	public function tableName()
	{
		return 'cs_mainrouting';
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->changeDb;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return  the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}